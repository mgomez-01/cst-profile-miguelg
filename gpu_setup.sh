#!/bin/bash -e

export DISTRO=ubuntu2004
export ARCHITECTURE=x86_64
sudo apt update
sudo add-apt-repository -y ppa:graphics-drivers
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/7fa2af80.pub
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/\ / > /etc/apt/sources.list.d/cuda.list"
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/machine-learning/repos/${DISTRO}/${ARCHITECTURE}\ / > /etc/apt/sources.list.d/cuda_learn.list"

sudo apt-get update
sudo apt-get install -y nvidia-headless-465 nvidia-utils-465
#sudo apt-get install -y nvidia-driver-465 || /bin/true
sudo modprobe nvidia
sleep 5
nvidia-smi

