#!/bin/bash

echo Establish an ssh connection to a computer with access to the CADE license
echo servers with the -D 8080 option to setup the SOCKS5 proxy for license
echo server access. Ping something \(like 8.8.8.8\) to keep the connection
echo alive. Press ctrl+A, D once this is done.
echo
echo Press enter to get the prompt.

read

screen -S socks_proxy bash

sudo systemctl restart redsocks

echo All done
