#!/bin/bash

screen -S Xvfb -d -m Xvfb -screen 0 1920x1080x24 :99
screen -S x11vnc -d -m x11vnc -display :99 -forever -nevershared
DISPLAY=:99 screen -S xfce4 -d -m xfce4-session
